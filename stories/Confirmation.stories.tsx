/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Confirmation } from '../src/components';

export default {
  title: 'Confirmation',
  component: Confirmation,
} as ComponentMeta<typeof Confirmation>;

const Template: ComponentStory<typeof Confirmation> = function ConfirmationTemplate(
  args
) {
  return <Confirmation {...args} />;
};

export const Default = Template.bind({});
Default.args = {
  title: 'Disable this issuer',
  message: 'Are you sure you want to disable this issuer?',
  handleChoice: () => {},
};
