/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Badge } from '../src/components';

export default {
  title: 'Badge',
  component: Badge,
} as ComponentMeta<typeof Badge>;

const Template: ComponentStory<typeof Badge> = function BadgeTemplate(args) {
  return <Badge {...args} />;
};

export const Default = Template.bind({});
Default.args = {};
