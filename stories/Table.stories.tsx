/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { TableColumnType, Table, Badge } from '../src/components';

type IssuerStatus = 'New' | 'Completed' | 'Pending' | 'Failed';

interface Issuer {
  businessName: string;
  legalName: string;
  typeOfBusiness: string;
  businessSubType: string;
  createdOn: string | Date;
  status: IssuerStatus;
}

const columns: TableColumnType<Issuer>[] = [
  {
    title: 'BUSINESS NAME',
    field: 'businessName',
    cellStyle: {
      color: 'blue',
      fontWeight: 'bold',
      textDecoration: 'none',
    },
    render: (rowData) => <a href="#!">{rowData.businessName}</a>,
  },
  {
    title: 'LEGAL NAME',
    field: 'legalName',
  },
  {
    title: 'TYPE OF BUSINESS',
    field: 'typeOfBusiness',
  },
  {
    title: 'BUSINESS SUBTYPE',
    field: 'businessSubType',
  },
  {
    title: 'Created On',
    field: 'createdOn',
    render: (rowData) => <span>{rowData.createdOn}</span>,
  },
  {
    title: 'Status',
    field: 'status',
    render: (rowData) => <Badge type={rowData.status} />,
  },
];

const defaultColumns: TableColumnType<Issuer>[] = [
  {
    title: 'BUSINESS NAME',
    field: 'businessName',
  },
  {
    title: 'LEGAL NAME',
    field: 'legalName',
  },
];

const sortColumns: TableColumnType<Issuer>[] = [
  {
    title: 'BUSINESS NAME',
    field: 'businessName',
    sorting: true,
  },
  {
    title: 'LEGAL NAME',
    field: 'legalName',
    sorting: true,
  },
];

const data: Issuer[] = [
  {
    businessName: 'Business name',
    legalName: 'Legal name',
    typeOfBusiness: 'Type of business',
    businessSubType: 'Subtype',
    createdOn: '26 Nov 2021, 14:13:31',
    status: 'New',
  },
  {
    businessName: 'Business name',
    legalName: 'Legal name',
    typeOfBusiness: 'Type of business',
    businessSubType: 'Subtype',
    createdOn: '26 Nov 2021, 14:13:31',
    status: 'Completed',
  },
  {
    businessName: 'Business name',
    legalName: 'Legal name',
    typeOfBusiness: 'Type of business',
    businessSubType: 'Subtype',
    createdOn: '26 Nov 2021, 14:13:31',
    status: 'New',
  },
  {
    businessName: 'Business name',
    legalName: 'Legal name',
    typeOfBusiness: 'Type of business',
    businessSubType: 'Subtype',
    createdOn: '26 Nov 2021, 14:13:31',
    status: 'Failed',
  },
  {
    businessName: 'Business name',
    legalName: 'Legal name',
    typeOfBusiness: 'Type of business',
    businessSubType: 'Subtype',
    createdOn: '26 Nov 2021, 14:13:31',
    status: 'New',
  },
  {
    businessName: 'Business name',
    legalName: 'Legal name',
    typeOfBusiness: 'Type of business',
    businessSubType: 'Subtype',
    createdOn: '26 Nov 2021, 14:13:31',
    status: 'Pending',
  },
  {
    businessName: 'Business name',
    legalName: 'Legal name',
    typeOfBusiness: 'Type of business',
    businessSubType: 'Subtype',
    createdOn: '26 Nov 2021, 14:13:31',
    status: 'Completed',
  },
  {
    businessName: 'Business name',
    legalName: 'Legal name',
    typeOfBusiness: 'Type of business',
    businessSubType: 'Subtype',
    createdOn: '26 Nov 2021, 14:13:31',
    status: 'Failed',
  },
];

export default {
  title: 'Table',
  component: Table,
} as ComponentMeta<typeof Table>;

const Template: ComponentStory<typeof Table> = function TableTemplate(args) {
  return <Table {...args} />;
};

export const Default = Template.bind({});
Default.args = {
  columns: defaultColumns,
  data,
  search: false,
};

export const WithSorting = Template.bind({});
WithSorting.args = {
  columns: sortColumns,
  data,
  search: false,
};

export const WithCellFormatters = Template.bind({});
WithCellFormatters.args = {
  columns,
  data,
};

export const WithSearch = Template.bind({});
WithSearch.args = {
  columns,
  data,
  search: true,
  handleSearchChange: () => {},
};

export const WithFilter = Template.bind({});
WithFilter.args = {
  columns,
  data,
  search: true,
  filtering: true,
  handleSearchChange: () => {},
};
