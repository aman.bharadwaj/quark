import React from "react";
import { Meta, Story } from "@storybook/react";

import { Typography, TypographyProps } from "../src/components";
import StoryLayout from "./StoryLayout";

const meta: Meta = {
  title: "Typography",
  component: Typography,
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

interface Props extends TypographyProps {
  darkMode: boolean;
}

const TypographyHeadings: Story<Props> = (args) => {
  return (
    <StoryLayout {...args} className="space-y-2">
      <Typography {...args} variant="h1">
        Display H1
      </Typography>
      <Typography {...args} variant="h2">
        Display H2
      </Typography>
      <Typography {...args} variant="h3">
        Display H3
      </Typography>
      <Typography {...args} variant="h4">
        Display H4
      </Typography>
    </StoryLayout>
  );
};

export const Heading = TypographyHeadings.bind({});

Heading.args = {
  customWeight: "regular",
  className: "",
  darkMode: false,
};

Heading.parameters = {
  controls: { exclude: ["variant", "customColor", "className"] },
};

const TypographyText: Story<Props> = (args) => {
  return (
    <StoryLayout {...args} className="space-y-2">
      <Typography {...args} variant="md">
        Text md
      </Typography>
      <Typography {...args} variant="sm">
        Text sm
      </Typography>
      <Typography {...args} variant="xs">
        Text xs
      </Typography>
    </StoryLayout>
  );
};

export const Text = TypographyText.bind({});

Text.args = {
  customWeight: "regular",
  className: "",
  darkMode: false,
};

Text.parameters = {
  controls: { exclude: ["variant", "customColor", "className"] },
};

const TypographyDynamic: Story<Props> = (args) => {
  const isHeading = args.variant.startsWith("h");

  return (
    <StoryLayout {...args} className="space-y-2">
      <Typography {...args}>
        {isHeading ? "Display" : "Text"} {args.variant} <br />{" "}
        {args.customWeight}
      </Typography>
    </StoryLayout>
  );
};

export const Dynamic = TypographyDynamic.bind({});

Dynamic.args = {
  variant: "h1",
  customColor: "",
  customWeight: "regular",
  className: "",
  darkMode: false,
};
