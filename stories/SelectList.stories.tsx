/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { SelectList, SelectOption } from '../src/components';

export default {
  title: 'Select List',
  component: SelectList,
} as ComponentMeta<typeof SelectList>;

const Template: ComponentStory<typeof SelectList> = function SelectListTemplate(
  args
) {
  return (
    <div style={{ width: '20rem' }}>
      <SelectList {...args} />;
    </div>
  );
};

const options: SelectOption[] = [
  {
    displayName: 'Hello',
    value: 'hello',
    key: 1,
  },
  {
    displayName: 'Hello',
    value: 'hello',
    key: 2,
  },
  {
    displayName: 'Hello',
    value: 'hello',
    key: 3,
  },
  {
    displayName: 'Hello',
    value: 'hello',
    key: 4,
  },
  {
    displayName: 'Hello',
    value: 'hello',
    key: 5,
  },
];

export const Default = Template.bind({});
Default.args = {
  placeholder: 'T+',
  options,
};
