/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { RiMapPin2Line } from 'react-icons/ri';
import { InputField } from '../src/components';

export default {
  title: 'InputField',
  component: InputField,
} as ComponentMeta<typeof InputField>;

const Template: ComponentStory<typeof InputField> = function InputFieldTemplate(
  args
) {
  return (
    <div style={{ width: '312px' }}>
      <InputField {...args} />
    </div>
  );
};

export const Default = Template.bind({});
Default.args = {
  placeholder: 'Enter Some Text',
};

export const WithIcon = Template.bind({});
WithIcon.args = {
  IconComponent: RiMapPin2Line,
  // iconPosition: 'left',
};

export const WithPrefix = Template.bind({});
WithPrefix.args = {
  prefix: '+91',
};

export const WithSuffix = Template.bind({});
WithSuffix.args = {
  suffix: '$',
};
