/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { DragDrop, FileRejection } from '../src/components';

export default {
  title: 'DragDrop',
  component: DragDrop,
} as ComponentMeta<typeof DragDrop>;

const Template: ComponentStory<typeof DragDrop> = function DragDropTemplate(
  args
) {
  return <DragDrop {...args} />;
};

export const Default = Template.bind({});
Default.args = {
  maxFiles: 1,
  minWidth: 200,
  maxWidth: 200,
  minHeight: 200,
  maxHeight: 200,
  handleAcceptedFiles: (acceptedFiles: File[]) => {
    console.log('acceptedFiles', acceptedFiles);
  },
  handleRejectedFiles: (rejectedFiles: FileRejection[]) => {
    console.log('rejectedFiles', rejectedFiles);
  },
};
