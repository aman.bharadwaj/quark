import React from "react";
import { Meta, Story } from "@storybook/react";
import StoryLayout from "./StoryLayout";

import { ColorBox } from "../src/components";
import { colors } from "../src/data";

const meta: Meta = {
  title: "Colors",
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

interface Props {
  darkMode: boolean;
}

const StoryColors: Story<Props> = (args) => (
  <StoryLayout
    {...args}
    className="grid grid-cols-3 gap-4 md:grid-cols-5 lg:grid-cols-5"
  >
    {colors.map((color) => (
      <ColorBox key={color.bgClass} color={color} />
    ))}
  </StoryLayout>
);

export const Default = StoryColors.bind({});
