/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { ExpandMore } from '../src/components';

export default {
  title: 'Expand More',
  component: ExpandMore,
} as ComponentMeta<typeof ExpandMore>;

const Template: ComponentStory<typeof ExpandMore> = function ExpandMoreTemplate(
  args
) {
  return <ExpandMore {...args} />;
};

export const Default = Template.bind({});
Default.args = {
  items: [
    {
      key: 1,
      text: 'Edit',
      attributes: {
        onClick: () => {},
      },
    },
    { key: 2, text: 'Remove' },
  ],
  expand: false,
};
