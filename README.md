### @nimbbl/quark

- Fully customizable React UI Components

### Usage

```tsx
import React from 'react';
import { Badge } from '@nimbbl/quark';
import '@nimbbl/quark/src/index.css';

function App() {
  return (
    <div>
      <Badge type="New" />
    </div>
  );
}

export default App;
```

### Install

```sh
npm i @nimbbl/quark
# or
yarn add @nimbbl/quark
```