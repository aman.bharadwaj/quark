import { IColor } from "../@interfaces";

export const colors: IColor[] = [
  {
    bgClass: "bg-gray-10",
    code: 10,
    hex: "#FFFFFF",
  },
  {
    bgClass: "bg-gray-20",
    code: 20,
    hex: "#FAFAFC",
  },
  {
    bgClass: "bg-gray-40",
    code: 40,
    hex: "#F6F8FC",
  },
  {
    bgClass: "bg-gray-60",
    code: 60,
    hex: "#F1F4F9",
  },
  {
    bgClass: "bg-gray-80",
    code: 80,
    hex: "#000000",
  },
  {
    bgClass: "bg-primary-10",
    code: 10,
    hex: "#F5E8FF",
  },
  {
    bgClass: "bg-primary-20",
    code: 20,
    hex: "#D8AAFD",
  },
  {
    bgClass: "bg-primary-40",
    code: 40,
    hex: "#AC54F0",
  },
  {
    bgClass: "bg-primary-60",
    code: 60,
    hex: "#8219D4",
  },
  {
    bgClass: "bg-primary-80",
    code: 80,
    hex: "#530094",
  },
  {
    bgClass: "bg-secondary-10",
    code: 10,
    hex: "#D3E1FE",
  },
  {
    bgClass: "bg-secondary-20",
    code: 20,
    hex: "#7EA5F8",
  },
  {
    bgClass: "bg-secondary-40",
    code: 40,
    hex: "#4D82F3",
  },
  {
    bgClass: "bg-secondary-60",
    code: 60,
    hex: "#0067DB",
  },
  {
    bgClass: "bg-secondary-80",
    code: 80,
    hex: "#0037B3",
  },
  {
    bgClass: "bg-tertiary-10",
    code: 10,
    hex: "#FFF8E5",
  },
  {
    bgClass: "bg-tertiary-20",
    code: 20,
    hex: "#FCCC75",
  },
  {
    bgClass: "bg-tertiary-40",
    code: 40,
    hex: "#FDAC42",
  },
  {
    bgClass: "bg-tertiary-60",
    code: 60,
    hex: "#FF8800",
  },
  {
    bgClass: "bg-tertiary-80",
    code: 80,
    hex: "#E57A00",
  },
  {
    bgClass: "bg-info-10",
    code: 10,
    hex: "#FFF5D5",
  },
  {
    bgClass: "bg-info-20",
    code: 20,
    hex: "#FFDE81",
  },
  {
    bgClass: "bg-info-40",
    code: 40,
    hex: "#FFCC00",
  },
  {
    bgClass: "bg-info-60",
    code: 60,
    hex: "#E5B800",
  },
  {
    bgClass: "bg-info-80",
    code: 80,
    hex: "#926B04",
  },
  {
    bgClass: "bg-success-10",
    code: 10,
    hex: "#E8FCF1",
  },
  {
    bgClass: "bg-success-20",
    code: 20,
    hex: "#A5E1BF",
  },
  {
    bgClass: "bg-success-40",
    code: 40,
    hex: "#219653",
  },
  {
    bgClass: "bg-success-60",
    code: 60,
    hex: "#00632B",
  },
  {
    bgClass: "bg-success-80",
    code: 80,
    hex: "#00401C",
  },
  {
    bgClass: "bg-error-10",
    code: 10,
    hex: "#FFEBEB",
  },
  {
    bgClass: "bg-error-20",
    code: 20,
    hex: "#FC9595",
  },
  {
    bgClass: "bg-error-40",
    code: 40,
    hex: "#D83232",
  },
  {
    bgClass: "bg-error-60",
    code: 60,
    hex: "#B01212",
  },
  {
    bgClass: "bg-error-80",
    code: 80,
    hex: "#8C0000",
  },
  {
    bgClass: "bg-text-10",
    code: 10,
    hex: "#FFFFFF",
  },
  {
    bgClass: "bg-text-20",
    code: 20,
    hex: "#CBD4E1",
  },
  {
    bgClass: "bg-text-40",
    code: 40,
    hex: "#94A3B8",
  },
  {
    bgClass: "bg-text-60",
    code: 60,
    hex: "#475569",
  },
  {
    bgClass: "bg-text-80",
    code: 80,
    hex: "#0F1A2A",
  },
];
