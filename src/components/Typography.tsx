import React from "react";
import classNames from "classnames";

type TypographyVariant = "xs" | "sm" | "md" | "h4" | "h3" | "h2" | "h1";

type TypographyWeightOptions = "regular" | "medium" | "semibold" | "bold";

type TypographyWeightValues =
  | "font-normal"
  | "font-medium"
  | "font-semibold"
  | "font-bold";

export interface TypographyProps {
  variant: TypographyVariant;
  customColor?: string;
  customWeight?: TypographyWeightOptions;
  className?: string;
  children: string | React.ReactNode;
}

const TypographyVariantClasses: Record<TypographyVariant, string> = {
  xs: "text-xs",
  sm: "text-sm",
  md: "text-md",
  h4: "text-h4",
  h3: "text-h3",
  h2: "text-h2",
  h1: "text-h1",
};

const TypographyWeightClasses: Record<
  TypographyWeightOptions,
  TypographyWeightValues
> = {
  regular: "font-normal",
  medium: "font-medium",
  semibold: "font-semibold",
  bold: "font-bold",
};

export const Typography = ({
  variant = "md",
  customColor,
  customWeight = "regular",
  className,
  children,
}: TypographyProps): JSX.Element => {
  const TypographyVariantClassName = TypographyVariantClasses[variant];
  const TypographyWeightClassName = TypographyWeightClasses[customWeight];
  const isHeading = variant.startsWith("h");
  const Component = (isHeading ? variant : "p") as keyof JSX.IntrinsicElements;

  return (
    <Component
      className={classNames(
        TypographyVariantClassName,
        TypographyWeightClassName,
        className,
        {
          ["tracking-tight"]: ["h1", "h2", "h3"].includes(variant),
          ["text-black dark:text-white"]: !customColor,
        },
        customColor,
      )}
    >
      {children}
    </Component>
  );
};
