/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-console */
/* eslint-disable no-param-reassign */
/* eslint-disable react/require-default-props */
import React, { useCallback } from 'react';
import {
  FileRejection as RFileRejection,
  useDropzone,
  FileError,
} from 'react-dropzone';
import { RiImage2Line } from 'react-icons/ri';
import './DragDrop.css';

export type FileRejection = RFileRejection;

export type DragDropSize = 'sm' | 'lg';

export interface DragDropProps {
  size?: DragDropSize;
  maxFiles?: number;
  minSize?: number;
  maxSize?: number;
  maxWidth?: number;
  minWidth?: number;
  maxHeight?: number;
  minHeight?: number;
  handleAcceptedFiles: (_acceptedFiles: File[]) => void;
  handleRejectedFiles: (_rejectedFiles: FileRejection[]) => void;
}

export function DragDrop({
  size = 'lg',
  maxFiles = 0,
  maxSize,
  minSize,
  maxWidth,
  minWidth,
  maxHeight,
  minHeight,
  handleAcceptedFiles,
  handleRejectedFiles,
}: DragDropProps) {
  const onDrop = useCallback(
    (acceptedFiles, fileRejections: FileRejection[]) => {
      handleAcceptedFiles(acceptedFiles);
      handleRejectedFiles(fileRejections);
    },
    []
  );

  const validator = useCallback((file) => {
    const image = new Image();
    if (
      (maxWidth && file.width > maxWidth) ||
      (minWidth && file.width < minWidth) ||
      (maxHeight && file.height > maxHeight) ||
      (minHeight && file.height < minHeight)
    ) {
      const error: FileError = {
        message: 'Invalid image dimensions',
        code: '1',
      };
      return error;
    }
    image.src = URL.createObjectURL(file);
    return null;
  }, []);

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    maxFiles,
    maxSize,
    minSize,
    validator,
    getFilesFromEvent: async (event: any) => {
      const files = event.target.files || event.dataTransfer.files;
      const promises: Promise<any>[] = [];
      let index = 0;
      for (const key in files) {
        if (Object.prototype.hasOwnProperty.call(files, key)) {
          const file = files[index];
          const promise = new Promise((resolve) => {
            const image = new Image();
            image.onload = () => {
              file.width = image.width;
              file.height = image.height;
              resolve(file);
            };
            URL.createObjectURL(file);
            image.src = URL.createObjectURL(file);
          });
          promises.push(promise);
          index++;
        }
      }
      return Promise.all(promises);
    },
  });

  return (
    <div className={`dropzone-${size || 'lg'}`} {...getRootProps()}>
      <input {...getInputProps()} />
      {isDragActive ? (
        <p>Drop the files here ...</p>
      ) : (
        <>
          <RiImage2Line className="dropzone-icon" />
          <p>
            Drag and drop here <br /> or
          </p>
          <p className="dropzone-link">Browse files</p>
        </>
      )}
    </div>
  );
}
