/* eslint-disable react/button-has-type */
/* eslint-disable react/require-default-props */
import React, { CSSProperties, HTMLProps } from 'react';
import { IconType } from 'react-icons';
import './Button.css';

export type ButtonVariant = 'primary' | 'secondary' | 'outline' | 'text';
export type ButtonSize = 'sm' | 'lg';
export type ButtonIconPosition = 'left' | 'right' | 'center' | 'none';
export type ButtonThemeColor = 'purple' | 'info';
export type ButtonShade = 10 | 20 | 40 | 60 | 80;
export type ButtonType = 'button' | 'reset' | 'submit';

export interface ButtonProps {
  text?: string;
  IconComponent?: IconType;
  type?: ButtonType;
  variant?: ButtonVariant;
  themeColor?: ButtonThemeColor;
  shade?: ButtonShade;
  size?: ButtonSize;
  iconPosition?: ButtonIconPosition;
  style?: CSSProperties;
  attributes?: HTMLProps<HTMLButtonElement>;
}

const getVariantClasses = (
  variant: ButtonVariant,
  themeColor: ButtonThemeColor,
  shade: number
): string => {
  switch (variant) {
    case 'primary':
      return `bg-${themeColor}-${shade} text-white-white`;
    case 'secondary':
      return 'bg-secondary text-secondary-text';
    case 'outline':
      return `bg-transparent text-${themeColor}-${shade} border border-${themeColor}-${shade}`;
    case 'text':
      return `bg-transparent border-none text-${themeColor}-${shade}`;
    default:
      return '';
  }
};

const getSizeClasses = (size: ButtonSize, icon = false): string => {
  switch (size) {
    case 'lg':
      return `lg-${icon ? 'icon' : 'btn'}`;
    case 'sm':
      return `sm-${icon ? 'icon' : 'btn'}`;
    default:
      return '';
  }
};

export function Button({
  text,
  IconComponent,
  type = 'button',
  variant = 'primary',
  themeColor = 'purple',
  shade = 60,
  size = 'lg',
  iconPosition = 'none',
  style = {},
  attributes = {},
}: ButtonProps) {
  const variantClasses = getVariantClasses(variant, themeColor, shade);
  const sizeClasses = getSizeClasses(size);
  const iconSizeClasses = getSizeClasses(size, true);

  // 'bg-purple-60 text-purple-60 bg-info-60 text-info-60 border-purple-60 border-info-60 border bg-transparent';

  return (
    <button
      className={`${variantClasses} ${sizeClasses} inline-flex justify-center items-center
      }`}
      style={{ ...style }}
      {...{
        ...attributes,
        type,
      }}
    >
      {iconPosition === 'center' ? (
        IconComponent && <IconComponent className={iconSizeClasses} />
      ) : (
        <>
          {iconPosition === 'left' && IconComponent && (
            <IconComponent className={iconSizeClasses} />
          )}
          {text}
          {iconPosition === 'right' && IconComponent && (
            <IconComponent className={iconSizeClasses} />
          )}
        </>
      )}
    </button>
  );
}
