import React, { LiHTMLAttributes } from 'react';
import { IconType } from 'react-icons';
import { RiArrowDownSFill } from 'react-icons/ri';
import './Sidebar.css';

export type NavItem = {
  id: string | number;
  Icon: IconType;
  text: string;
  children?: NavItem[];
  secondary?: boolean;
  visited?: boolean;
  attributes?: LiHTMLAttributes<HTMLLIElement>;
};

export interface SidebarProps {
  navItems: NavItem[];
  logoUrl?: string;
  logo?: JSX.Element;
  profilePhotoUrl: string;
}

export function Sidebar({
  navItems,
  logoUrl,
  profilePhotoUrl,
  logo,
}: SidebarProps) {
  return (
    <div className="sidebar">
      <div className="logo">
        {logoUrl && <img src={logoUrl} alt="logo" />} {logo && logo}{' '}
      </div>
      <ul className="nav-item-list">
        {navItems.map(
          ({ id, Icon, text, secondary, visited, children, attributes }) =>
            !secondary && (
              <div key={id}>
                <li
                  className={`nav-item ${
                    visited ? 'visited-children-item' : ''
                  }`}
                  {...(attributes || {})}
                >
                  <div className="nav-item-prefix-container">
                    <Icon
                      className={`nav-item-icon ${
                        visited ? 'visited-children-icon' : ''
                      }`}
                    />
                    {text}
                  </div>
                  {children && children.length && (
                    <RiArrowDownSFill
                      className={`nav-item-suffix-icon ${
                        visited ? 'visited-children-icon' : ''
                      }`}
                    />
                  )}
                </li>
                <div className="nav-children-item-list">
                  {children?.map(({ Icon, text, visited, attributes }) => (
                    <li
                      className={`nav-item ${
                        visited ? 'visited-children-item' : ''
                      }`}
                      {...(attributes || {})}
                    >
                      <div className="children-item-content">
                        <Icon
                          className={`nav-item-icon ${
                            visited ? 'visited-children-icon' : ''
                          }`}
                        />
                        {text}
                      </div>
                    </li>
                  ))}
                </div>
              </div>
            )
        )}
        <div className="divider" />
        {navItems.map(
          ({ id, Icon, text, secondary, children, visited, attributes }) =>
            secondary && (
              <div key={id}>
                <li className="nav-item" {...(attributes || {})}>
                  <div className="nav-item-prefix-container">
                    <Icon className="nav-item-icon" />
                    {text}
                  </div>
                  {children && children.length && (
                    <RiArrowDownSFill
                      className={`nav-item-suffix-icon ${
                        visited ? 'visited-children-icon' : ''
                      }`}
                    />
                  )}
                </li>
                <div className="nav-children-item-list">
                  {children?.map(({ id, Icon, text, visited, attributes }) => (
                    <li
                      className={`nav-item ${
                        visited ? 'visited-children-item' : ''
                      }`}
                      key={id}
                      {...(attributes || {})}
                    >
                      <div className="children-item-content">
                        <Icon
                          className={`nav-item-icon ${
                            visited ? 'visited-children-icon' : ''
                          }`}
                        />
                        {text}
                      </div>
                    </li>
                  ))}
                </div>
              </div>
            )
        )}
      </ul>
      <div className="profile">
        <img src={profilePhotoUrl} alt="profile" />
        <span>Profile</span>
      </div>
    </div>
  );
}
