/* eslint-disable no-unused-vars */
/* eslint-disable react/require-default-props */
import React from 'react';
import './Confirmation.css';

export interface ConfirmationProps {
  title: string;
  message: string;
  yesText?: string;
  noText?: string;
  handleChoice: (_accept: boolean) => void;
}

export function Confirmation({
  title,
  message,
  yesText,
  noText,
  handleChoice,
}: ConfirmationProps) {
  return (
    <div className="confirmation">
      <div className="title">{title}</div>
      <p className="message">{message}</p>
      <div className="btns-container">
        <button
          onClick={() => handleChoice(true)}
          type="button"
          className="btn success"
        >
          {yesText || 'Yes'}
        </button>
        <button
          onClick={() => handleChoice(false)}
          type="button"
          className="btn error"
        >
          {noText || 'No'}
        </button>
      </div>
    </div>
  );
}
