import React, { LiHTMLAttributes, useEffect, useState } from 'react';
import { RiMore2Fill } from 'react-icons/ri';
import './ExpandMore.css';

export interface ListItem {
  key: string | number;
  text: string;
  attributes?: LiHTMLAttributes<HTMLLIElement>;
}

export interface ExpandMoreProps {
  items: ListItem[];
  expand: boolean;
}

const expandMoreIconId = 'expand-more-icon';

export function ExpandMore({ items, expand }: ExpandMoreProps) {
  const [isOpen, setIsOpen] = useState(expand);

  useEffect(() => {
    setIsOpen(expand);
  }, [expand]);

  useEffect(() => {
    const escClose = (e: KeyboardEvent) => {
      if (e.key === 'Escape') {
        setIsOpen(false);
      }
    };
    const clickClose = (e: MouseEvent) => {
      const el: any = e.target;
      if (el.id !== expandMoreIconId) {
        setIsOpen(false);
      }
    };

    window.addEventListener('keydown', escClose);
    window.addEventListener('click', clickClose);
    return () => {
      window.removeEventListener('keydown', escClose);
      window.removeEventListener('click', clickClose);
    };
  }, []);

  return (
    <div className="dropdown">
      <RiMore2Fill
        id={expandMoreIconId}
        className="more-icon"
        onClick={() => setIsOpen(true)}
      />
      {isOpen && (
        <ul className="dropdown-menu absolute">
          {items.map((el) => (
            <li
              key={el.key}
              className="dropdown-menu-item"
              {...(el.attributes ?? {})}
            >
              <span>{el.text}</span>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
}
