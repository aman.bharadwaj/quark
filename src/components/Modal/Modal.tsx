import { CSSProperties } from '@material-ui/core/styles/withStyles';
import React, { useEffect, useState } from 'react';
import RModal from 'react-modal';
import './Modal.css';

export interface ModalProps {
  jsx: JSX.Element;
  show: boolean;
  appElement: string;
  contentLabel?: string;
  modalContentStyles?: CSSProperties;
  modalOverlayStyles?: CSSProperties;
  onClose?: () => void;
}

export function Modal({
  jsx,
  show,
  appElement,
  contentLabel,
  modalContentStyles = {},
  modalOverlayStyles = {},
  onClose,
}: ModalProps) {
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    RModal.setAppElement(appElement);
  }, [appElement]);

  useEffect(() => {
    setIsOpen(show);
  }, [show]);

  const modalStyles: RModal.Styles = {
    content: {
      width: '420px',
      height: '500px',
      top: '50%',
      left: '50%',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      ...(modalContentStyles || {}),
    },
    overlay: {
      background: 'rgba(0, 0, 0, 0.6)',
      ...(modalOverlayStyles || {}),
    },
  };

  const handleShowModal = (open: boolean = true) => {
    setIsOpen(open);
  };

  return (
    <RModal
      isOpen={isOpen}
      onRequestClose={() => {
        handleShowModal(false);
        if (onClose) {
          onClose();
        }
      }}
      contentLabel={contentLabel}
      style={modalStyles}
      shouldCloseOnEsc
    >
      {jsx}
    </RModal>
  );
}
