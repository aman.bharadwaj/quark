/* eslint-disable no-nested-ternary */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/require-default-props */
import React, { CSSProperties, HTMLProps } from 'react';
import { RiArrowDownLine } from 'react-icons/ri';
import './SelectList.css';

// textLabel, caption, error message
// varients
// 1. Grey text, grey border (simple)
// 2. Dark text, blue/primary border (primary)
// 3. Dark text, grey border (Secondary)
// 4. Dark text, Red border (Error/Warning?)
// 5. Grey text, Dull bg (Disabled?)

// Icon, Dropdown, Icon+Dropdown

export type SelectListVariant =
  | 'primary'
  | 'secondary'
  | 'error'
  | 'disabled'
  | 'simple';

export type SelectListCaptionType = 'text' | 'error';

export interface SelectOption {
  value: string | number | readonly string[];
  displayName: string | number;
  key: string | number;
}

// take id in props
export interface SelectListProps {
  id: string;
  options: SelectOption[];
  label?: string;
  placeholder?: string;
  caption?: string;
  captionType?: SelectListCaptionType;
  variant?: SelectListVariant;
  style?: CSSProperties;
  attributes?: HTMLProps<HTMLSelectElement>;
}

const getVariantClasses = (variant: SelectListVariant): string => {
  switch (variant) {
    case 'primary':
      return `border-info-80 border text-black-primary font-normal`;
    case 'secondary':
      return `border-secondary border text-black-primary font-normal`;
    case 'error':
      return `border-red-40 border text-black-primary font-normal`;
    case 'disabled':
      return `bg-secondary border-secondary border text-secondary-text font-normal`;
    case 'simple':
      return `border-secondary border text-secondary-text font-normal`;
    default:
      return '';
  }
};

const getCaptionClasses = (captionType: SelectListCaptionType): string => {
  switch (captionType) {
    case 'text':
      return 'text-black-secondary';
    case 'error':
      return 'text-red-60';
    default:
      return '';
  }
};

export function SelectList({
  id,
  label,
  options,
  caption = '',
  captionType = 'text',
  variant = 'primary',
  style = {},
  attributes = {},
  placeholder,
}: SelectListProps) {
  const variantClasses = getVariantClasses(variant);
  const captionClasses = getCaptionClasses(captionType);

  return (
    <div className="flex flex-col">
      {/* Label */}
      <label className="select-label" htmlFor={id}>
        {label}
      </label>
      {/* Main Input */}
      <div
        id="main-input"
        className="w-0 relative flex items-center text-black-primary"
      >
        {/* Icon */}
        <div className="right-1 absolute m-3 pointer-events-none">
          <RiArrowDownLine />
        </div>
        <select
          id={id}
          className={`${variantClasses} rounded w-full py-3 pl-3 pr-8`}
          style={{ ...style }}
          {...{ ...attributes }}
        >
          <option value="">{placeholder || 'Select'}</option>
          {options.map((opt) => (
            <option key={opt.key} value={opt.value}>
              {opt.displayName}
            </option>
          ))}
        </select>
      </div>
      {/* Caption */}
      <div>
        {caption && <p className={`${captionClasses} pt-1 pl-2`}>{caption}</p>}
      </div>
    </div>
  );
}
