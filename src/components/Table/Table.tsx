/* eslint-disable react/require-default-props */
import React from 'react';
import MaterialTable, { Column } from 'material-table';
import { CSSProperties } from '@material-ui/core/styles/withStyles';

export type TableColumnType<T extends object> = Column<T>;

export interface TableProps {
  data: any[];
  columns: Column<any>[];
  search?: boolean;
  filtering?: boolean;
  // eslint-disable-next-line no-unused-vars
  handleSearchChange?: (_searchText: string) => void;
}

const headerStyle: CSSProperties = {
  height: '40px',
  padding: '12px 16px',
  fontStyle: 'normal',
  fontWeight: 600,
  fontSize: '12px',
  lineHeight: '16px',
  textTransform: 'uppercase',
  color: '#828282',
  background: '#ffffff',
  boxShadow: '0px 1px 0px #E4E5E8',
  borderBottom: '1px solid #E0E0E0',
  borderTop: '1px solid #E0E0E0',
};

const rowStyle: CSSProperties = {
  padding: '10px 16px',
  backgroundColor: '#ffffff',
  boxShadow: '0px 1px 0px #F1F2F3',
  height: '40px',
  fontWeight: 'normal',
  fontStyle: 'normal',
  fontSize: '14px',
  lineHeight: '20px',
  color: '#333333',
  textAlign: 'right',
};

export function Table({
  columns,
  data,
  search = false,
  filtering = false,
  handleSearchChange,
}: TableProps) {
  return (
    <MaterialTable
      style={{
        width: '1168px',
        height: '528px',
        border: '1px solid #E0E0E0',
        borderRadius: '4px',
      }}
      data={data}
      columns={columns}
      options={{
        search,
        filtering,
        showTitle: false,
        headerStyle,
        rowStyle,
      }}
      onSearchChange={handleSearchChange}
    />
  );
}
