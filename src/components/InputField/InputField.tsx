/* eslint-disable no-nested-ternary */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/require-default-props */
import React, { CSSProperties, HTMLProps } from 'react';
import { IconType } from 'react-icons';
import './InputField.css';

// textLabel, caption, error message
// varients
// 1. Grey text, grey border (simple)
// 2. Dark text, blue/primary border (primary)
// 3. Dark text, grey border (Secondary)
// 4. Dark text, Red border (Error/Warning?)
// 5. Grey text, Dull bg (Disabled?)

// Icon, Dropdown, Icon+Dropdown

export type InputFieldVariant =
  | 'primary'
  | 'secondary'
  | 'error'
  | 'disabled'
  | 'simple';
export type InputFieldIconPosition = 'left' | 'right';
export type InputFieldDropdownPosition = 'left' | 'right' | 'center' | 'none';
export type InputFieldType = 'text' | 'selector';
export type InputFieldCaptionType = 'text' | 'error';

// take id in props
export interface InputFieldProps {
  id: string;
  IconComponent?: IconType;
  label?: string;
  placeholder?: string;
  type?: InputFieldType;
  caption?: string;
  captionType?: InputFieldCaptionType;
  suffix?: string;
  prefix?: string;
  iconPosition?: InputFieldIconPosition;
  dropdownPosition?: InputFieldDropdownPosition;
  variant?: InputFieldVariant;
  style?: CSSProperties;
  attributes?: HTMLProps<HTMLInputElement>;
}

const getVariantClasses = (variant: InputFieldVariant): string => {
  switch (variant) {
    case 'primary':
      return `border-info-80 border text-black-primary font-normal`;
    case 'secondary':
      return `border-secondary border text-black-primary font-normal`;
    case 'error':
      return `border-red-40 border text-black-primary font-normal`;
    case 'disabled':
      return `bg-secondary border-secondary border text-secondary-text font-normal`;
    case 'simple':
      return `border-secondary border text-secondary-text font-normal`;
    default:
      return '';
  }
};

const getIconClasses = (iconPosition: InputFieldIconPosition): string => {
  switch (iconPosition) {
    case 'left':
      return '';
    case 'right':
      return 'right-1';
    default:
      return '';
  }
};

const getCaptionClasses = (captionType: InputFieldCaptionType): string => {
  switch (captionType) {
    case 'text':
      return 'text-black-secondary';
    case 'error':
      return 'text-red-60';
    default:
      return '';
  }
};

export function InputField({
  id,
  IconComponent,
  label,
  placeholder,
  type = 'text',
  caption,
  captionType = 'text',
  suffix,
  prefix,
  iconPosition = 'left',
  // dropdownPosition = 'right',
  variant = 'primary',
  style = {},
  attributes = {},
}: InputFieldProps) {
  const variantClasses = getVariantClasses(variant);
  const iconClasses = getIconClasses(iconPosition);
  const captionClasses = getCaptionClasses(captionType);

  return (
    <div className="flex flex-col">
      {/* Label */}
      <div>
        <label className="py-2 text-black-primary font-semibold" htmlFor={id}>
          {label}
        </label>
      </div>
      {/* Main Input */}
      <div
        id="main-input"
        className="w-0 relative flex  items-center text-black-primary"
      >
        {/* Icon */}
        {IconComponent && (
          <div className={`${iconClasses} absolute m-3 pointer-events-none`}>
            <IconComponent />
          </div>
        )}
        {/* Prefix */}
        {prefix && (
          <div className="absolute">
            <p>{prefix}</p>
          </div>
        )}
        <input
          id={id}
          className={`${variantClasses} rounded w-full py-3 ${
            IconComponent
              ? 'pl-6 pr-3'
              : prefix
              ? 'pl-8 pr-3'
              : suffix
              ? 'pl-3 pr-8'
              : iconPosition === 'right'
              ? 'pl-3 pr-8'
              : 'px-3'
          } `}
          style={{ ...style }}
          placeholder={placeholder}
          type={type}
          {...{ ...attributes }}
          autoComplete="off"
        />
        {/* Suffix */}
        {suffix && (
          <div className="absolute right-3">
            <p>{suffix}</p>
          </div>
        )}
      </div>
      {/* Caption */}
      <div>
        {caption && <p className={`${captionClasses} pt-1 pl-2`}>{caption}</p>}
      </div>
    </div>
  );
}
